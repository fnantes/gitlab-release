import os from 'os';
import fs from 'fs-extra';
import chai from 'chai';
import git from 'nodegit';
import which from 'which';
import process from 'process';
import childProcess from 'child_process';
import chaiAsPromised from 'chai-as-promised';
import * as release from '../lib';
import ExecutionError from '../lib/ExecutionError';

chai.should();
chai.use(chaiAsPromised);

describe('description', () => {
  it('description should pass through markdown', () => {
    const markdown = 'Hello, world!';
    return release.description({markdown}).should.become(markdown);
  });

  it('should reject empty descriptions', () => {
    const markdown = '';
    return release.description({markdown}).should.be.rejectedWith(TypeError);
  });

  it.skip('interactively ask the user for the release description', function () {
    this.timeout(30000);
    const expected = 'test';
    const boilerplate = `${os.EOL}${os.EOL}<!-- Enter '${expected}' to pass the test -->`;
    return release.description({boilerplate}).should.become(expected);
  });
});

describe('gitlabEndpoint', () => {
  it('resolves correct GitLab endpoint', () => {
    return release.gitlabEndpoint().should.become('https://gitlab.vcatechnology.com');
  });
});

describe('gitlabProject', () => {
  it('resolves correct GitLab project', () => {
    return release.gitlabProject().should.become('gitlab-release');
  });
});

describe('runCommand', () => {
  it('can run node', () => {
    const cmd = [which.sync('node'), '--version'];
    let stdout = '';
    const callback = buffer => {
      const data = buffer.toString('utf-8').trim();
      if (data) {
        stdout += data;
      }
    };
    return release.runCommand(cmd, {stdout: callback})
      .then(code => {
        code.should.equal(0);
        stdout.length.should.not.equal(0, `No output received from running '${cmd.join(' ')}'`);
        stdout.should.match(/^v[0-9]+\.[0-9]+\.[0-9]+$/);
      });
  });

  it('can raises an ExceptionError', () => {
    const cmd = [process.execPath, '--hfdoaguoabgosag'];
    return release.runCommand(cmd).should.be.rejectedWith(ExecutionError);
  });
});

describe('ExecutionError', () => {
  it('has a code property', () => {
    const error = new ExecutionError(10, ['cmd']);
    error.code.should.equal(10);
  });
  it('can be converted to a string', () => {
    const error = new ExecutionError(10, ['cmd']);
    error.toString().should.equal(`The 'cmd' command returned '10'`);
  });
});

describe('find', () => {
  it('resolves multiple glob patterns', () => {
    const expected = [
      'test/index.js',
      'lib/.CHANGELOG.md.in',
      'lib/.release-description.md.in'
    ];
    return release.find(['test/**/*.js', 'lib/.*.md.in']).should.become(expected);
  });
});

describe('git', () => {
  it('can create, add and remove git files', function () {
    this.timeout(10000);
    const repopath = `${__dirname}/fixtures/repo`;
    const filepath = `${repopath}/dist/test.md`;
    const remove = new Promise((resolve, reject) => {
      fs.remove(repopath, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
    return remove
      .then(() => release.writeFile(filepath, 'whoop'))
      .then(() => release.statFile(filepath))
      .then(() => git.Repository.init(repopath, 0))
      .then(() => release.gitAdd(['dist/**/*'], repopath))
      .then(() => release.gitCommit('added', repopath))
      .then(() => release.gitCreateTag('tag', 'message', repopath))
      .then(() => release.gitRemove(['dist/**/*'], repopath))
      .then(() => release.gitCommit('removed', repopath))
      .then(() => git.Repository.open(repopath))
      .then(repo => repo.getTagByName('tag'));
  });
});

describe('updateChangelog', () => {
  const data = {
    date: '2000-01-01',
    project: 'web/gitlab-release',
    description: 'description',
    version: {
      from: '0.9.0',
      to: '1.0.0'
    },
    issues: [],
    merges: []
  };

  const expected =
    `# Changelog${os.EOL}` +
    `${os.EOL}` +
    `## [v1.0.0](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v1.0.0) (2000-01-01)${os.EOL}` +
    `${os.EOL}` +
    `[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.9.0...v1.0.0)${os.EOL}` +
    `${os.EOL}` +
    `${data.description.replace(/^(#+ .+)$/mg, '##$1')}${os.EOL}` +
    `${os.EOL}` +
    `### Closed Issues${os.EOL}` +
    `${os.EOL}` +
    `_None_${os.EOL}` +
    `${os.EOL}` +
    `### Merged Branches${os.EOL}` +
    `${os.EOL}` +
    `_None_${os.EOL}`;

  it('create new changelog', () => {
    const filepath = `${__dirname}/fixtures/previous-changelog.md`;
    return release.unlink(filepath)
    .catch(err => {
      if (!(err && err.errno && (err.code === 'ENOENT'))) {
        throw err;
      }
    })
    .then(() => release.updateChangelog({data, filepath}))
    .then(() => release.readFile(filepath))
    .should.become(expected);
  });

  it('update existing changelog', () => {
    const extra = `${os.EOL}old changelog stuff should be left alone`;
    const filepath = `${__dirname}/fixtures/previous-changelog.md`;
    return release.writeFile(filepath, `# Changelog${os.EOL}${extra}`)
    .then(() => release.updateChangelog({data, filepath}))
    .then(() => release.readFile(filepath))
    .should.become(expected + extra);
  });
});

describe('templateData', () => {
  const endpoint = 'https://gitlab.vcatechnology.com';
  const name = 'gitlab-release';
  const from = '0.1.1';
  const to = '0.1.2';
  const token = '01234567890123456789';

  it('invalid GitLab tokens should be rejected', () => {
    const invalid = 'Hello, world!';
    return release.templateData({endpoint, name, from, to, token: invalid}).should.be.rejectedWith(TypeError);
  });

  it('invalid GitLab token types should be rejected', () => {
    const invalid = null;
    return release.templateData({endpoint, name, from, to, token: invalid}).should.be.rejectedWith(TypeError);
  });

  it(`invalid 'from' versions should be rejected`, () => {
    const invalid = 'Hello, world!';
    return release.templateData({endpoint, name, from: invalid, to, token}).should.be.rejectedWith(TypeError);
  });

  it(`invalid 'from' version types should be rejected`, () => {
    const invalid = [1, 2, 3];
    return release.templateData({endpoint, name, from: invalid, to, token}).should.be.rejectedWith(TypeError);
  });

  it(`invalid 'to' versions should be rejected`, () => {
    const invalid = 'Hello, world!';
    return release.templateData({endpoint, name, from, to: invalid, token}).should.be.rejectedWith(TypeError);
  });

  it(`invalid 'to' version types should be rejected`, () => {
    const invalid = [1, 2, 3];
    return release.templateData({endpoint, name, from, to: invalid, token}).should.be.rejectedWith(TypeError);
  });

  if (process.env.GITLAB_API_TOKEN) {
    it('should retrieve GitLab template data', () => {
      return release.templateData({endpoint, name, from, to})
        .then(data => {
          data.should.have.property('date');
          data.should.have.property('issues').with.length(0);
          data.should.have.property('merges').with.length(3);
          data.should.have.property('project');
          data.should.have.property('version').with.property('from');
          data.should.have.property('version').with.property('to');
          data.should.have.property('gitlab').with.property('endpoint');
          data.should.have.property('gitlab').with.property('project');
        });
    });
  }
});

describe('gitlabNotes', () => {
  const endpoint = 'https://gitlab.vcatechnology.com';
  const project = 'gitlab-release';
  const version = '0.0.1';
  const notes = 'description';
  const token = '01234567890123456789';

  it('invalid GitLab tokens should be rejected', () => {
    const invalid = 'Hello, world!';
    return release.gitlabNotes({endpoint, project, version, notes, token: invalid}).should.be.rejectedWith(TypeError);
  });

  it('invalid GitLab token types should be rejected', () => {
    const invalid = null;
    return release.gitlabNotes({endpoint, project, version, notes, token: invalid}).should.be.rejectedWith(TypeError);
  });

  it('invalid versions should be rejected', () => {
    const invalid = 'Hello, world!';
    return release.gitlabNotes({endpoint, project, version: invalid, notes, token}).should.be.rejectedWith(TypeError);
  });

  it('invalid version types should be rejected', () => {
    const invalid = [1, 2, 3];
    return release.gitlabNotes({endpoint, project, version: invalid, notes, token}).should.be.rejectedWith(TypeError);
  });

  it('empty release notes should be rejected', () => {
    const invalid = '';
    return release.gitlabNotes({endpoint, project, version, notes: invalid, token}).should.be.rejectedWith(TypeError);
  });
});

describe('gitCurrentBranch', () => {
  it('should be able to return the current git branch', () => {
    return new Promise((resolve, reject) => {
      const cmd = ['rev-parse', '--abbrev-ref', 'HEAD'];
      childProcess.spawn('git', cmd)
        .on('exit', status => {
          if (status) {
            reject(new Error(`Failed to execute git command process: ${status}: ${cmd.join(', ')}`));
          }
        })
        .on('error', error => {
          reject(error);
        })
        .stdout.on('data', buffer => resolve(buffer.toString().trimRight()));
    })
      .then(expected => release.gitCurrentBranch().then(branch => [expected, branch]))
      .then(([expected, branch]) => branch.should.equal(expected));
  });
});

describe('writeFile', () => {
  it('can write a nested file', () => {
    const path = `${__dirname}/fixtures/some/nested/file.txt`;
    const content = 'Hello, world!';
    return release.writeFile(path, content)
    .then(() => release.readFile(path))
    .should.become(content);
  });
});

describe('statFile', () => {
  it('can retrieve file status', () => {
    const path = `${__dirname}/index.js`;
    return release.statFile(path)
    .should.eventually.have.property('isFile');
  });
});

describe('dist', () => {
  const type = 'minor';
  const notes = 'description';
  const token = '01234567890123456789';

  it('invalid release names should be rejected', () => {
    const invalid = 'blah';
    return release.dist({type: invalid, notes, token}).should.be.rejectedWith(TypeError);
  });

  it('invalid release types should be rejected', () => {
    const invalid = 12394;
    return release.dist({type: invalid, notes, token}).should.be.rejectedWith(TypeError);
  });

  it('empty release notes should be rejected', () => {
    return release.dist({type, notes: '', token}).should.be.rejectedWith(TypeError);
  });

  it('invalid GitLab tokens should be rejected', () => {
    const invalid = 'Hello, world!';
    return release.dist({type, notes, token: invalid}).should.be.rejectedWith(TypeError);
  });

  it('invalid GitLab token types should be rejected', () => {
    const invalid = null;
    return release.dist({type, notes, token: invalid}).should.be.rejectedWith(TypeError);
  });
});
