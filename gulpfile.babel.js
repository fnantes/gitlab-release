import path from 'path';
import del from 'del';
import process from 'process';
import {Instrumenter} from 'isparta';
import gulp from 'gulp';
import loadPlugins from 'gulp-load-plugins';
import 'babel-register';
import 'babel-polyfill';
import * as release from './lib';

// Load all of our Gulp plugins
const $ = loadPlugins();

gulp.task('static', () => {
  return gulp.src('**/*.js')
    .pipe($.excludeGitignore())
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());
});

gulp.task('nsp', cb => {
  $.nsp({package: path.resolve('package.json')}, cb);
});

gulp.task('pre-test', () => {
  return gulp.src('lib/**/*.js')
    .pipe($.excludeGitignore())
    .pipe($.istanbul({
      includeUntested: true,
      instrumenter: Instrumenter
    }))
    .pipe($.istanbul.hookRequire());
});

gulp.task('test', ['pre-test', 'static'], cb => {
  let mochaErr;

  gulp.src('test/**/*.js')
    .pipe($.plumber())
    .pipe($.mocha({reporter: 'spec'}))
    .on('error', err => {
      mochaErr = err;
      if (mochaErr.showStack) {
        cb(mochaErr);
      }
    })
    .pipe($.istanbul.writeReports())
    .on('end', () => {
      cb(mochaErr);
    });
});

gulp.task('watch', () => {
  gulp.watch(['lib/**/*.js', 'test/**'], ['test']);
});

gulp.task('babel', ['bin', 'lib']);

gulp.task('lib', ['clean'], () => {
  return gulp.src('lib/**/*.js')
    .pipe($.babel())
    .pipe(gulp.dest('dist/lib'));
});

gulp.task('bin', ['clean'], () => {
  return gulp.src('bin/**/*')
    .pipe($.babel())
    .pipe(gulp.dest('dist/bin'));
});

gulp.task('templates', ['clean'], () => {
  return gulp.src('lib/**/.*.md.in')
    .pipe(gulp.dest('dist/lib'));
});

gulp.task('dist', ['babel', 'templates']);

gulp.task('clean', () => {
  return del('dist');
});

const releaseDataJson = '.release-data.json';

gulp.task('preversion', () => {
  const argv = JSON.parse(process.env.npm_config_argv);
  const type = argv.remain[0];
  return release.preversion({type})
    .then(data => release.writeJson(releaseDataJson, data));
});

gulp.task('postversion', () => {
  return release.readJson(releaseDataJson)
    .then(data => release.postversion({notes: data.description}))
    .then(() => release.unlink(releaseDataJson));
});

gulp.task('prepublish', ['dist']);
gulp.task('default', ['static', 'test']);
