# Changelog

## [v0.2.13](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.13) (2017-08-29)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.12...v0.2.13)

### Fixed crash when adding many files

Enabled thread safety for git add
Update dependencies
Ensure the promise object construction completes after a reject

### Closed Issues

_None_

### Merged Branches

  - Enable thread safety for git add [\#18](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/18)

  - Update Project Dependencies [\#17](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/17)

## [v0.2.12](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.12) (2016-10-13)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.11...v0.2.12)

Corrects the slash dependency for the binary

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.11](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.11) (2016-10-13)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.10...v0.2.11)

Adds the binary to the package JSON

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.10](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.10) (2016-10-13)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.9...v0.2.10)

Adds a binary and new APIs

### `gitlab-release`

The project can now be installed globally and a `gitlab-release` binary is
available.

```
gitlab-release [major|minor|patch] --pre-command <cmd> --post-command <cmd> --file-path <path> --patterns <globs>
```

This will:

  - Fire up `${EDITOR}` to enter a MarkDown release description
  - Run the pre-command if specified
  - Add the `patterns`
  - Bump the version in the `file-path` JSON file
  - Tag the project
  - Remove the `patterns`
  - Create a GitLab release note

For example, releasing polymer projects can be done as so:

```
gitlab-release [major|minor|patch] --pre-command 'polymer build' --file-path bower.json --patterns 'build/**/*,build/**/.*'
```

Rather than specifying the commands each time, it is possible to specify a `.grrc` file to define the optional arguments
for a project. The above command is equivalent to the following `.grrc` file:

```json
{
  "filePath": "bower.json",
  "preCommand": ["polymer", "build"],
  "patterns": ["build/**/*", "build/**/.*"]
}
```

### New APIs

Various new APIs have been added to support the binary. They are exported as
part of the interface but are mostly lower level filesystem plumbing APIs.

### Extra unit testing

The `git` APIs are now unit tested to provide some extra coverage.

### Closed Issues

_None_

### Merged Branches

  - 'dist' Improvements [\#16](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/16)

  - Binary [\#15](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/15)

## [v0.2.9](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.9) (2016-07-29)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.8...v0.2.9)

`gitRemove` fixed to remove files the same way they are added in `gitAdd`.

### Closed Issues

_None_

### Merged Branches

  - Bump dependency versions [\#13](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/13)

## [v0.2.8](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.8) (2016-07-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.7...v0.2.8)

Only files are added in `gitAdd`. This resolves issues when a glob pattern
matches folders.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.7](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.7) (2016-07-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.6...v0.2.7)

Updates the dependencies of the module.

### Closed Issues

_None_

### Merged Branches

  - Bump dependency versions [\#13](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/13)

## [v0.2.6](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.6) (2016-05-24)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.5...v0.2.6)

Bump nodegit version to 0.13.0

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.5](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.5) (2016-04-08)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.4...v0.2.5)

Fixes up the change log rendering. The change log template had an incorrect data
key that meant the closed merge requests were not showing up in the
`CHANGELOG.md`, this is now fixed. The project is now tested on Mac OSX as well.

### Closed Issues

_None_

### Merged Branches

  - Changelog fix [\#12](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/12)

  - Mac OSX [\#11](https://gitlab.vcatechnology.com/web/gitlab-release/merge_requests/11)

## [v0.2.4](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.4) (2016-03-23)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.3...v0.2.4)

The `writeFile` API now creates the parent directory.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.3](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.3) (2016-03-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.2...v0.2.3)

Adds intructions for the `dist` script integration

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.2](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.2) (2016-03-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.1...v0.2.2)

Resolves a bug in the `dist` script. The `dist` script now also resolves with
the released version which helps logging the released version.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.1](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.1) (2016-03-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.2.0...v0.2.1)

Allows the user of the scripts to override the GitLab endpoint and project name

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.2.0](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.2.0) (2016-03-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.1.3...v0.2.0)

The project learnt a `dist` script that commits the files in the `dist` folder
into the tagged version. This is especially useful for package managers that
use the git repository as the distribution method, such as `bower`.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.1.3](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.1.3) (2016-03-15)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.1.2...v0.1.3)

Updates the `dist` to include the needed template files and brings along
Windows support for the project.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.1.2](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.1.2) (2016-03-11)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.1.1...v0.1.2)

This release provides no new APIs but sets up the project deployment and
build server. A major bug fix landed for determining the git tag date so
that issues and merges get returned correctly in the `templateData`
function.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.1.1](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.1.1) (2016-03-11)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.1.0...v0.1.1)

The preversion script now uses the current git branch rather than
defaulting to `master`

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.1.0](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.1.0) (2016-03-11)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.0.2...v0.1.0)

This release provides a full release process for an `npm` project. The usage
instructions are in the `README.md` file in the repository. This project uses
the `preversion` and `postvesion` functions to release itself.

### Closed Issues

_None_

### Merged Branches

_None_

## [v0.0.2](https://gitlab.vcatechnology.com/web/gitlab-release/tree/v0.0.2) (2016-03-11)

[Full Changelog](https://gitlab.vcatechnology.com/web/gitlab-release/compare/v0.0.1...v0.0.2)

The first version released with the preversion and postversion scripts

### Closed Issues

_None_

### Merged Branches

_None_
