import ExtendableError from 'es6-error';

export default class extends ExtendableError {
  constructor(status, cmd) {
    super();
    Object.defineProperties(this, {
      status: {
        value: status,
        enumerable: true
      },
      cmd: {
        value: cmd,
        enumerable: true
      }
    });
    Object.freeze(cmd);
  }

  get code() {
    return this.status;
  }

  toString() {
    return `The '${this.cmd.join(' ')}' command returned '${this.status}'`;
  }
}
