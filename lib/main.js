import yargs from 'yargs';
import InvalidArgumentError from './InvalidArgumentError';
import * as release from './index.js';

export default function (argv) {
  argv.splice(0, 2);

  return release.readJson('.grrc')
    .catch(err => {
      if (err.code !== 'ENOENT') {
        throw err;
      }

      return {};
    })
    .then(config => {
      const reCommand = /(?:[^\s"]+|"[^"]*")+/g;
      const command = arg => (arg instanceof Array) ? arg : arg.match(reCommand);
      const patterns = list => (list instanceof Array) ? list : list.split(',');

      const args = yargs
        .usage('usage: $0 <type>')
        .config(config)
        .option('file-path', {
          type: 'string',
          default: 'package.json',
          describe: 'The file to bump the version in',
          global: true
        })
        .option('pre-command', {
          coerce: command,
          describe: 'A command to run before the release occurs',
          global: true
        })
        .option('post-command', {
          coerce: command,
          describe: 'A command to run after the release occurs',
          global: true
        })
        .option('patterns', {
          coerce: patterns,
          describe: 'The patterns to add as part of the release',
          default: 'dist/**/*,dist/**/.*',
          global: true
        })
        .command('patch', 'patch version release', y => y.config(config))
        .command('minor', 'minor version release', y => y.config(config))
        .command('major', 'major version release', y => y.config(config))
        .updateStrings({
          'Commands:': 'type:'
        })
        .wrap(null)
        .demand(1)
        .strict()
        .help('help')
        .exitProcess(false)
        .fail(msg => {
          throw new InvalidArgumentError(msg);
        })
        .parse(argv);
      args.type = args._[0];
      return args;
    })
    .then(args => {
      if (!args.help) {
        return release.ignored(args);
      }
    });
}
