import p from 'path';
import fs from 'fs';
import os from 'os';
import url from 'url';
import tmp from 'tmp';
import glob from 'glob';
import git from 'nodegit';
import gitlab from 'gitlab';
import semver from 'semver';
import slash from 'slash';
import mkdirp from 'mkdirp-then';
import process from 'process';
import mustache from 'mustache';
import childProcess from 'child_process';
import ApiError from './ApiError';
import ExecutionError from './ExecutionError';

/* Ensure we can do git add on multiple files on different threads
 * see https://github.com/nodegit/nodegit/issues/1084 for info
 **/
git.setThreadSafetyStatus(git.THREAD_SAFETY.ENABLED_FOR_ASYNC_ONLY);

/**
 * Performs a release of a GitLab project that needs to adds a set of ignored files as part of the release. This script
 * does the following things:
 *   - retrieves release data from GitLab for the release such as issues closed and resolved merge requests
 *   - asks the user for the release description
 *   - adds the ignored files specified in `patterns`
 *   - bumps the package version
 *   - updates the changelog using the release description and the GitLab data
 *   - commits the changed files
 *   - creates a version tag for the project
 *   - removes the `patterns`
 *   - pushes the commits and tag
 *   - creates GitLab release notes to accompany the new git tag
 * @param {String} type must be one of `major`, `minor` or `patch`
 * @param {String} [notes] a Markdown formatted description of the release
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String|Promise} [template.description] mustache template for the editor description boilerplate
 * @param {String|Promise} [template.changelog] mustache template used to update the changelog
 * @param {String} [endpoint] the URI of the GitLab API. Will be determined from `filePath` if not provided
 * @param {String} [project] the name of the project in GitLab. Will be determined from `filePath` if not provided
 * @param {String} [filePath] the path to the file to be read the version from. Will be determined from `package.json` if not provided
 * @param {String} [preCommand] a command to run before the tag is applied
 * @param {String} [postCommand] a command to run after the tag is applied
 * @param {Array} [patterns] the patterns to add as part of the release
 * @returns {Promise} the released version
 */
export function ignored({
    type,
    notes,
    token = process.env.GITLAB_API_TOKEN,
    changelog = 'CHANGELOG.md',
    template = {},
    endpoint,
    project,
    filePath = 'package.json',
    preCommand,
    postCommand,
    patterns
  }) {
  if (!patterns) {
    throw new TypeError('Must provide a list of patterns to release');
  }

  return runCommand(preCommand)
    .then(() => {
      // Check arguments
      if (!/^[A-Za-z0-9]{20}$/.exec(token || '')) {
        throw new TypeError(`The GitLab token is invalid: ${token}`);
      }
      const types = ['major', 'minor', 'patch'];
      if (types.indexOf(type || '') === -1) {
        throw new TypeError(`The release type must be one of: ${types.join(', ')}`);
      }
    })
    .then(() => {
      return ((typeof notes) === 'string') ? [null, {}] :
      descriptionBoilerplate({type, token, template: template.description, endpoint, project, filePath});
    })
    .then(([boilerplate, data]) => {
      return description({markdown: notes, boilerplate})
        .then(markdown => {
          data.description = markdown;
          return data;
        });
    })
    .then(data => updateChangelog({data, filepath: changelog, template: template.changelog}))
    .then(data => forward(data, bumpVersion(type, filePath)))
    .then(data => forward(data, gitAdd([filePath, changelog])))
    .then(data => forward(data, gitAdd(patterns)))
    .then(data => forward(data, gitCommit(`${data.version.to} release`)))
    .then(data => forward(data, gitCreateTag(`v${data.version.to}`, /^(.*)[\r\n.]?/.exec(data.description)[1])))
    .then(data => forward(data, gitRemove(patterns)))
    .then(data => forward(data, gitCommit('Remove released files folder')))
    .then(data => gitCurrentBranch().then(branch => [data, branch]))
    .then(([data, branch]) => gitPush({branches: [branch], tags: [`v${data.version.to}`]}).then(() => data))
    .then(data => {
      return gitlabNotes({
        endpoint: data.gitlab.endpoint,
        name: data.gitlab.project,
        version: data.version.to,
        notes: data.description,
        token
      }).then(() => data);
    })
    .then(data => runCommand(postCommand).then(() => data))
    .then(data => data.version.to);
}

/**
 * Performs a release of a GitLab node project that needs the `dist` folder to be added for each tagged version and
 * be removed whilst the project is in development. This script does the following things:
 *   - retrieves release data from GitLab for the release such as issues closed and resolved merge requests
 *   - asks the user for the release description
 *   - adds the ignored `dist` folder
 *   - bumps the package version
 *   - updates the changelog using the release description and the GitLab data
 *   - commits the changed files
 *   - creates a version tag for the project
 *   - removes the `dist` folder
 *   - pushes the commits and tag
 *   - creates GitLab release notes to accompany the new git tag
 * This means that the project will always have a `dist` folder checked in for each release but not for the development
 * versions.
 * @param {String} type must be one of `major`, `minor` or `patch`
 * @param {String} [notes] a Markdown formatted description of the release
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String|Promise} [template.description] mustache template for the editor description boilerplate
 * @param {String|Promise} [template.changelog] mustache template used to update the changelog
 * @param {String} [endpoint] the URI of the GitLab API. Will be determined from `filePath` if not provided
 * @param {String} [project] the name of the project in GitLab. Will be determined from `filePath` if not provided
 * @param {String} [filePath] the path to the file to be read the version from. Will be determined from `package.json` if not provided
 * @param {String} [preCommand] a command to run before the `dist` folder is released
 * @param {String} [postCommand] a command to run after the `dist` folder has been released
 * @returns {Promise} the released version
 */
export function dist({
    type,
    notes,
    token = process.env.GITLAB_API_TOKEN,
    changelog = 'CHANGELOG.md',
    template = {},
    endpoint,
    project,
    filePath = 'package.json',
    preCommand,
    postCommand
  }) {
  return ignored({
    type,
    notes,
    token,
    changelog,
    template,
    endpoint,
    project,
    filePath,
    preCommand,
    postCommand,
    patterns: ['dist/**/*', 'dist/**/.*']
  });
}

/**
 * A function that can be ran during the `npm preversion` script to describe the release and update the changelog
 * @param {String} type must be one of `major`, `minor` or `patch`
 * @param {String} [notes] a Markdown formatted description of the release
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String|Promise} [template.description] mustache template for the editor description boilerplate
 * @param {String|Promise} [template.changelog] mustache template used to update the changelog
 * @param {String} [endpoint] the URI of the GitLab API. Will be determined from `package.json` if not provided
 * @param {String} [project] the name of the project in GitLab. Will be determined from `package.json` if not provided
 * `postversion` function can use the same data
 * @returns {Promise} resolves with the release data
 */
export function preversion({
    type,
    notes,
    token = process.env.GITLAB_API_TOKEN,
    changelog = 'CHANGELOG.md',
    template = {},
    endpoint,
    project
  }) {
  return promisify()
    .then(() => {
      // Check arguments
      if (!/^[A-Za-z0-9]{20}$/.exec(token || '')) {
        throw new TypeError(`The GitLab token is invalid: ${token}`);
      }
      const types = ['major', 'minor', 'patch'];
      if (types.indexOf(type || '') === -1) {
        throw new TypeError(`The release type must be one of: ${types.join(', ')}`);
      }
    })
    .then(() => {
      return ((typeof notes) === 'string') ? [null, {}] :
      descriptionBoilerplate({type, token, template: template.description, endpoint, project});
    })
    .then(([boilerplate, data]) => {
      return description({markdown: notes, boilerplate})
        .then(markdown => {
          data.description = markdown;
          return data;
        });
    })
    .then(data => updateChangelog({data, filepath: changelog, template: template.changelog}))
    .then(data => gitAdd([changelog]).then(() => data));
}

/**
 * A function that can be ran during the `npm postversion` script to create GitLab release notes
 * @param {String} notes a Markdown formatted description of the release
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String} [endpoint] the URI of the GitLab API. Will be determined from `package.json` if not provided
 * @param {String} [project] the name of the project in GitLab. Will be determined from `package.json` if not provided
 * @returns {Promise} resolves with the release data result of the API call
 */
export function postversion({
    notes,
    token = process.env.GITLAB_API_TOKEN,
    endpoint,
    project}) {
  return gitCurrentBranch()
    .then(branch => packageJson().then(json => [branch, json.version]))
    .then(([branch, version]) => gitPush({branches: [branch], tags: [`v${version}`]}).then(() => version))
    .then(version => gitlabEndpoint(endpoint).then(endpoint => [version, endpoint]))
    .then(([version, endpoint]) => {
      return gitlabProject(project).then(project => [version, endpoint, project]);
    })
    .then(([version, endpoint, project]) => {
      return gitlabNotes({endpoint, name: project, version, notes, token});
    });
}

/**
 * Runs a command
 * @param {Array} cmd the command to run, if `null` or `undefined` the function will just resolve with `0`
 * @param {Function} [stdout] a function that is invoked when `stdout` data is provided
 * @param {Stream} [stdout.buffer] the `stdout` buffer data
 * @param {ChildProcess} [stdout.process] the process that is running
 * @param {Function} [stderr] a function that is invoked when `stderr` data is provided
 * @param {Stream} [stdout.buffer] the `stderr` buffer data
 * @param {ChildProcess} [stdout.process] the process that is running
 * @returns {Promise} resolves with the status code
 */
export function runCommand(cmd, {stdout = () => {}, stderr = () => {}} = {}) {
  if (!cmd) {
    return Promise.resolve(0);
  }

  return new Promise((resolve, reject) => {
    const p = childProcess.spawn(cmd[0], cmd.slice(1));

    p.stdout.on('data', buffer => stdout(buffer, p));

    p.stderr.on('data', buffer => stderr(buffer, p));

    p.on('close', status => {
      if (status) {
        reject(new ExecutionError(status, cmd));
      } else {
        resolve(status);
      }
    });

    p.on('error', error => {
      reject(error);
    });
  });
}

/**
 * Turns a raw JavaScript object into a promise
 * @param {Object} object the JavaScript object to resolve the promise with
 * @returns {Promise} resolves with the `object`
 */
function promisify(object) {
  return Promise.resolve(object);
}

/**
 * Forwards a value through a promise chain
 * @param {Object} passthrough the JavaScript object that will be returned from the promise chain
 * @param {Object|Promise} object the promise chain or object to promisify to pass the object through
 * @returns {Promise} resolves with the `passthrough`
 */
function forward(passthrough, object) {
  return promisify(object).then(() => passthrough);
}

/**
 * Returns the current branch name for a repository
 * @param {String} repo the repository path to add the file to
 * @returns {Promise} the working directory branch
 */
export function gitCurrentBranch(repo = process.cwd()) {
  return git.Repository.open(repo)
    .then(repository => repository.getCurrentBranch())
    .then(branch => branch.shorthand());
}

/**
 * Finds files by using a array of glob patterns
 * @param {Array} patterns the file patterns to search for
 * @returns {Promise} the array of files found
 */
export function find(patterns) {
  return Promise.all(patterns.map(pattern => {
    return new Promise((resolve, reject) => {
      glob(pattern, (err, files) => {
        if (err) {
          reject(err);
        }
        resolve(files);
      });
    });
  }))
  .then(array => [].concat(...array));
}

/**
 * Adds files to the git repository
 * @param {Array} patterns the file patterns to add to the git repository
 * @param {String} repo the repository path to add the files to
 * @returns {Promise} the files are added upon resolution
 */
export function gitAdd(patterns, repo = process.cwd()) {
  return git.Repository.open(repo)
    .then(repository => repository.refreshIndex())
    .then(index => find(patterns.map(pattern => p.join(repo, pattern))).then(files => [index, files]))
    .then(([index, possibles]) => {
      return Promise.all(possibles.map(possible => statFile(possible).then(status => status.isFile())))
        .then(files => [index, possibles.filter((possible, i) => files[i])]);
    })
    .then(([index, files]) => [index, files.sort((a, b) => a.length < b.length)])
    .then(([index, files]) => [index, files.map(file => p.relative(repo, file))])
    .then(([index, files]) => [index, files.map(slash)])
    .then(([index, files]) => Promise.all(files.map(file => index.addByPath(file))).then(() => index))
    .then(index => index.write());
}

/**
 * Removes files from the git repository
 * @param {Array} patterns the file patterns to remove from the git repository
 * @param {String} repo the repository path to remove the files from
 * @returns {Promise} the files are removed upon resolution
 */
export function gitRemove(patterns, repo = process.cwd()) {
  return git.Repository.open(repo)
    .then(repository => repository.refreshIndex())
    .then(index => find(patterns.map(pattern => p.join(repo, pattern))).then(files => [index, files]))
    .then(([index, possibles]) => {
      return Promise.all(possibles.map(possible => statFile(possible).then(status => status.isFile())))
        .then(files => [index, possibles.filter((possible, i) => files[i])]);
    })
    .then(([index, files]) => [index, files.sort((a, b) => a.length < b.length)])
    .then(([index, files]) => [index, files.map(file => p.relative(repo, file))])
    .then(([index, files]) => [index, files.map(slash)])
    .then(([index, files]) => Promise.all(files.map(file => index.removeByPath(file))).then(() => index))
    .then(index => index.write());
}

/**
 * Commits a files in a git repository
 * @param {String} msg the commit message
 * @param {String} repo the repository path to add the file to
 * @returns {Promise} the files will be committed upon resolution
 */
export function gitCommit(msg, repo = process.cwd()) {
  return git.Repository.open(repo)
    .then(repository => repository.getHeadCommit().then(parent => [repository, parent, repository.defaultSignature()]))
    .then(([repository, parent, signature]) => {
      return repository.refreshIndex()
        .then(index => index.writeTree())
        .then(oid => [repository, parent, signature, oid]);
    })
    .then(([repository, parent, signature, oid]) => {
      const parents = parent ? [parent] : [];
      return repository.createCommit('HEAD', signature, signature, msg, oid, parents);
    });
}

/**
 * Creates an annotated tag in the repository
 * @param {String} name the name of the tag to create
 * @param {String} msg the commit message
 * @param {String} repo the repository path to add the file to
 * @returns {Promise} the tag will be created upon resolution
 */
export function gitCreateTag(name, msg, repo = process.cwd()) {
  return git.Repository.open(repo)
  .then(repository => repository.getHeadCommit().then(oid => [repository, oid]))
  .then(([repository, oid]) => repository.createTag(oid, name, msg));
}

/**
 * Pushes new commits to the remote
 * @param {String} remote the git remote to push the changes to
 * @param {Array} branches the branches to push
 * @param {Array} tags the tags that should be pushed to the remote
 * @param {String} repo the repository path to add the file to
 * @returns {Promise} the repository will be insync with the repository upon resolution
 */
export function gitPush({branches = [], tags = [], remote = 'origin', repo = process.cwd()}) {
  return git.Repository.open(repo)
    .then(repository => {
      return repository.getRemote(remote);
    })
    .then(remote => {
      const refs = branches.map(branch => `refs/heads/${branch}:refs/heads/${branch}`);
      refs.push(...tags.map(tag => `refs/tags/${tag}:refs/tags/${tag}`));
      return remote.push(refs,
        {
          callbacks: {
            credentials: (url, userName) => {
              return git.Cred.sshKeyFromAgent(userName);
            }
          }
        }
      );
    });
}

/**
 * Returns a description boilerplate by doing template data replacement
 * @param {String} type must be one of `major`, `minor` or `patch`
 * @param {String|Promise} template the mustache template data to fill out
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String} [repo=process.cwd()] the location of the git repository
 * @param {String} [endpoint] the URI of the GitLab API. Will be determined from `package.json` if not provided
 * @param {String} [project] the name of the project in GitLab. Will be determined from `package.json` if not provided
 * @param {String} [filePath] the path to the file to be read the version from. Will be determined from `package.json` if not provided
 * @returns {Promise} resolves with `[boilerplate, data]`
 */
export function descriptionBoilerplate({
    type,
    template = readFile(`${__dirname}/.release-description.md.in`),
    token = process.env.GITLAB_API_TOKEN,
    repo = process.cwd(),
    endpoint,
    project,
    filePath = 'package.json'
  }) {
  return gitlabEndpoint(endpoint, filePath)
    .then(endpoint => {
      return gitlabProject(project, filePath).then(project => [endpoint, project]);
    })
    .then(([endpoint, project]) => {
      return readJson(filePath)
        .then(json => json.version)
        .then(from => {
          if (from === undefined) {
            from = '0.0.0';
          }
          const to = semver.inc(from, type);
          return [endpoint, project, from, to];
        });
    })
    .then(([endpoint, name, from, to]) => templateData({endpoint, name, from, to, token, repo}))
    .then(data => promisify(template).then(tmpl => [tmpl, data]))
    .then(([tmpl, data]) => {
      const escapeFn = mustache.escape;
      mustache.escape = text => text;
      const rendered = mustache.render(tmpl, data);
      mustache.escape = escapeFn;
      return [rendered, data];
    });
}

/**
 * Performs a promisified GitLab API request
 * @private
 * @example
 * return apiRequest(api.projects.search, 'Failed to find the project in the GitLab projects', name, null)
 *   .then(projects => {
 *     const project = projects.filter(p => { return p.name === name;})[0];
 *     if (!project) { throw new Error('Failed to find the project in the GitLab projects'); }
 *     return project;
 *   })
 * @param {Function} fn the gitlab API function to promisify
 * @param {String} msg the error message to raise if the request fails
 * @param {Iterable} args the arguments to forward on to the `fn`
 * @returns {Promise} the parsed JSON data returned from the API
 */
function apiRequest(fn, msg, ...args) {
  return new Promise((resolve, reject) => {
    fn(...args, data => {
      if (data) {
        resolve(data);
      } else {
        reject(new ApiError(msg));
      }
    });
  });
}

/**
 * Resolves the date and time that a git tag was made
 * @param {String} tag the tag to search for
 * @param {String} [path=process.cwd()] the location of the git repository
 * @returns {Promise} will resolve to the JavaScript `Date` object for the tag
 */
function getGitTagDate(tag, path = process.cwd()) {
  return git.Repository.open(path)
    .then(repo => Promise.all([repo, repo.getTagByName(tag)]))
    .then(([repo, tag]) => repo.getCommit(tag.target().id()))
    .then(commit => new Date(commit.time() * 1000));
}

/**
 * Unlinks a file with a promise
 * @private
 * @param {String} path the filesystem path to unlink
 * @returns {Promise} resolves when the file is unlinked
 */
export function unlink(path) {
  return new Promise((resolve, reject) => {
    fs.unlink(path, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

/**
 * Reads a file with a promise
 * @private
 * @param {String} filepath the filepath to read
 * @param {String} [encoding='utf-8'] the encoding to use to read the file
 * @returns {Promise} the data in the file
 */
export function readFile(filepath, encoding = 'utf-8') {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, encoding, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

/**
 * Reads and parses a JSON file
 * @param {String} filepath the filepath to read
 * @returns {Promise} the JSON data in the file
 */
export function readJson(filepath) {
  return readFile(filepath)
    .then(json => JSON.parse(json))
    .catch(err => {
      if (err instanceof SyntaxError) {
        throw new SyntaxError(`Failed to parse '${filepath}': ${err.message}`);
      } else {
        throw err;
      }
    });
}

/**
 * Writes a file with a promise
 * @private
 * @param {String} filepath the filepath to write
 * @param {String} data the data to write
 * @param {String} [encoding='utf-8'] the encoding to use to write the file
 * @returns {Promise} resolves once the file is written
 */
export function writeFile(filepath, data, encoding = 'utf-8') {
  return mkdirp(p.dirname(filepath))
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.writeFile(filepath, data, encoding, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    });
}

/**
 * Retrieves the filesystem status about a path
 * @private
 * @param {String} filepath the filepath to check
 * @returns {Promise} resolves once the result is known
 */
export function statFile(filepath) {
  return new Promise((resolve, reject) => {
    fs.open(filepath, 'r', (err, fd) => {
      if (err) {
        reject(err);
      } else {
        resolve(fd);
      }
    });
  })
  .then(fd => {
    return new Promise((resolve, reject) => {
      fs.fstat(fd, (err, stats) => {
        if (err) {
          reject(err);
        } else {
          resolve(stats);
        }
      });
    });
  });
}

/**
 * Writes JSON data to a file
 * @param {String} filepath the filepath to write
 * @param {Object} object the JavaScript object to convert to JSON
 * @returns {Promise} resolves once the file is written
 */
export function writeJson(filepath, object) {
  return writeFile(filepath, JSON.stringify(object, null, 2) + os.EOL);
}

/**
 * Reads an parses the `package.json`
 * @returns {Promise} resolves to the gitlab URL endpoint
 */
export function packageJson() {
  return readJson('package.json');
}

/**
 * Determines the gitlab endpoint from the `homepage` key in the `package.json`
 * @param {String} endpoint the endpoint to pass through
 * @param {String} [filePath] the path to the file to be read the version from. Will be determined from `package.json` if not provided
 * @returns {Promise} resolves to the gitlab URL endpoint
 */
export function gitlabEndpoint(endpoint, filePath = 'package.json') {
  return endpoint ? promisify(endpoint) : readJson(filePath)
    .then(data => url.parse(data.homepage))
    .then(parsed => {
      const slashes = parsed.slashes ? '//' : '';
      return `${parsed.protocol}${slashes}${parsed.host}`;
    });
}

/**
 * Determines the gitlab project from the `name` key in the `package.json`
 * @param {String} project the name to pass through
 * @param {String} [filePath] the path to the file to be read the version from. Will be determined from `package.json` if not provided
 * @returns {Promise} resolves to the gitlab project name
 */
export function gitlabProject(project, filePath = 'package.json') {
  return project ? promisify(project) : readJson(filePath)
    .then(data => data.name);
}

/**
 * Reads the `version` key from the `package.json`
 * @returns {Promise} resolves to the version string
 */
export function projectVersion() {
  return packageJson()
    .then(data => data.version);
}

/**
 * Reads the `version` key from the `package.json`
 * @param {String} type must be one of `major`, `minor` or `patch`
 * @param {String} filePath the location of the JSON file to bump the `version` key of
 * @returns {Promise} resolves to the version string
 */
export function bumpVersion(type, filePath = 'package.json') {
  return readJson(filePath)
    .then(json => {
      const types = ['major', 'minor', 'patch'];
      if (types.indexOf(type || '') === -1) {
        throw new TypeError(`The release type must be one of: ${types.join(', ')}`);
      }
      const from = json.version ? json.version : '0.0.0';
      const to = semver.inc(from, type);
      json.version = to;
      return writeJson(filePath, json)
        .then(() => [from, to]);
    });
}

/**
 * A regular expression that matches simple semantic versions
 * @private
 * @const
 */
const reVersion = /^[0-9]+\.[0-9]+\.[0-9]+$/;

/**
 * Retrieves the release template data. This returns any issues that were closed and merges that occurred since a
 * previous release.
 * @example
 * import mustache from 'mustache';
 * const template =
 *   '{{version.from}} -> {{version.to}}\n' +
 *   '{{#issues#}}\n' +
      '#{{iid}} {{title}} {{url}}\n' +
 *   '{{/issues#}}';
 * templateData('vca.js', 0.0.1', '0.0.2')
 *   .then(data => {
 *     return mustache.render(template, data);
 *   });
 * @param {String} endpoint the URI of the GitLab API
 * @param {String} name the name of the project to get the template information for
 * @param {String} [from] the previous semantic version to get the GitLab data for
 * @param {String} [to] the current semantic version to get GitLab data up until
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @param {String} [repo=process.cwd()] the location of the git repository
 * @returns {Promise} resolves with the template data filled in from GitLab
 * @throws {TypeError} the parameters were incorrect
 */
export function templateData({
    endpoint,
    name,
    from,
    to,
    token = process.env.GITLAB_API_TOKEN,
    repo = process.cwd()
  }) {
  return new Promise((resolve, reject) => {
    // Check arguments
    if (!/^[A-Za-z0-9]{20}$/.exec(token || '')) {
      reject(new TypeError(`The GitLab token is invalid: ${token}`));
      return;
    }
    if (!reVersion.exec(from || '')) {
      reject(new TypeError(`The 'from' version is invalid: ${from}`));
      return;
    }
    if (!reVersion.exec(to || '')) {
      reject(new TypeError(`The 'to' version is invalid: ${to}`));
      return;
    }

    // Create the API object
    const api = gitlab({
      url: endpoint,
      token
    });

    // Get the template data
    return apiRequest(api.projects.search, 'Failed to retrieve the project list from GitLab', name, null)
      .then(projects => {
        const project = projects.filter(p => p.name === name)[0];

        if (!project) {
          throw new Error(`Failed to find the ${name} project in the GitLab projects`);
        }

        return project;
      })
      .then(project => {
        const params = {state: 'closed'};
        return apiRequest(api.projects.issues.list, 'Failed to retrieve issues from GitLab', project.id, params)
          .then(issues => {
            const projectPath = project.path_with_namespace; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            return [project, issues.map(i => {
              i.url = `${endpoint}/${projectPath}/issues/${i.iid}`;
              return i;
            })];
          });
      })
      .then(([project, issues]) => {
        const fn = api.projects.merge_requests.list; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        const params = {state: 'merged'};
        return apiRequest(fn, 'Failed to retrieve MRs from GitLab', project.id, params)
          .then(mergeRequests => {
            const projectPath = project.path_with_namespace; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            return [project, issues, mergeRequests.map(m => {
              m.url = `${endpoint}/${projectPath}/merge_requests/${m.iid}`;
              return m;
            })];
          });
      })
      .then(([project, issues, mergeRequests]) => {
        return getGitTagDate(`v${from}`, repo)
          .catch(() => Date.parse(0))
          .then(since => [project, issues, mergeRequests, since]);
      })
      .then(([project, issues, mergeRequests, since]) => {
        return getGitTagDate(`v${to}`, repo)
          .catch(() => new Date())
          .then(until => [project, issues, mergeRequests, since, until]);
      })
      .then(([project, issues, mergeRequests, since, until]) => {
        const projectPath = project.path_with_namespace; // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
        const data = {
          gitlab: {
            endpoint,
            project: name
          },
          date: (new Date()).toISOString().slice(0, 10),
          project: projectPath,
          version: {
            from: (from === '0.0.0') ? null : from,
            to
          },
          issues: issues.filter(o => {
            const updatedAt = Date.parse(o.updated_at); // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            return (o.state === 'closed') && (since <= updatedAt) && (updatedAt < until);
          }),
          merges: mergeRequests.filter(o => {
            const updatedAt = Date.parse(o.updated_at); // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
            return (o.state === 'merged') && (since <= updatedAt) && (updatedAt < until);
          })
        };

        resolve(data);
      })
      .catch(reject);
  });
}

/**
 * Updates the changelog inserting data in a defined template
 * @param {Object} data the data to use to update the changelog
 * @param {String} [filepath='CHANGELOG.md'] the filepath to update
 * @param {String|Promise} [template] the template to use to update the changelog
 * @returns {Promise} upon completion the changelog will be updated and the `data` is returned
 */
export function updateChangelog({
    data,
    filepath = 'CHANGELOG.md',
    template = readFile(`${__dirname}/.CHANGELOG.md.in`)
  }) {
  return readFile(filepath)
    .catch(err => {
      if (!(err && err.errno && (err.code === 'ENOENT'))) {
        throw err;
      }
      return '# Changelog';
    })
    .then(changelog => Promise.all([changelog, promisify(template)]))
    .then(([changelog, tmpl]) => {
      const escapeFn = mustache.escape;
      mustache.escape = text => text;
      const before = data.description;
      data.description = before.replace(/^(#+ .+)$/mg, '##$1');
      const rendered = mustache.render(tmpl, data);
      data.description = before;
      mustache.escape = escapeFn;
      return changelog.replace(/(# Changelog)(\r\n?|\n)?/, `$1${os.EOL}${os.EOL}${rendered}`);
    })
    .then(changelog => {
      return writeFile(filepath, changelog);
    })
    .then(() => data);
}

/**
 * Determines the release notes. If `markdown` is provided then it is passed through. Otherwise the users editor is
 * opened with the `boilerplate` to enter the release description.
 * @param {String} [markdown] a Markdown formatted description of the release
 * @param {String} [boilerplate] the boilerplate that will be shown in
 * @param {String} [editor] the editor to pop up to the user
 * @returns {Promise} the `markdown` will be passed through or the user will be prompted with their editor to enter the
 * description
 */
export function description({
    markdown,
    boilerplate =
      `${os.EOL}` +
      `${os.EOL}` +
      `<!-- Enter a markdown release description -->${os.EOL}` +
      `<!-- Comments, like this, will be removed -->${os.EOL}`,
    editor = process.env.EDITOR || 'vim'
  }) {
  return (((typeof markdown) === 'string') ?
    promisify(markdown) :
    new Promise((resolve, reject) => {
      tmp.file((err, filepath) => {
        if (err) {
          reject(err);
        } else {
          resolve(filepath);
        }
      });
    })
    .then(filepath => {
      return writeFile(filepath, boilerplate)
        .then(() => filepath);
    })
    .then(filepath => {
      return new Promise((resolve, reject) => {
        childProcess.spawn(editor, [filepath], {stdio: 'inherit'})
          .on('exit', status => {
            if (status) {
              reject(new Error(`Failed to execute editor process: ${status}`));
            } else {
              resolve(filepath);
            }
          })
          .on('error', error => {
            reject(error);
          });
      });
    })
    .then(readFile)
    .then(markdown => {
      return markdown.replace(/<!-- .+?(?=-->)-->(\r\n?|\n)?/g, '');
    })
  )
  .then(markdown => {
    markdown = markdown.trimRight();
    if (!markdown) {
      throw new TypeError('Must provide a non-empty release description');
    }
    return markdown;
  });
}

/**
 * Adds release notes to a GitLab release. This is usually performed once a version tag has been created in the
 * repository and pushed to the remote.
 * @param {String} endpoint the URI of the GitLab API
 * @param {String} name the name of the project to get the template information for
 * @param {String} version the semantic version of the tag that the release notes should be attached to
 * @param {String} notes the markdown formatted release notes
 * @param {String} [token=GITLAB_API_TOKEN] the GitLab API token to use to retrieve data about the project
 * @returns {Promise} the result of the API request
 */
export function gitlabNotes({
    endpoint,
    name,
    version,
    notes,
    token = process.env.GITLAB_API_TOKEN
  }) {
  return new Promise((resolve, reject) => {
    // Check arguments
    if (!/^[A-Za-z0-9]{20}$/.exec(token || '')) {
      reject(new TypeError(`The GitLab token is invalid: ${token}`));
    }
    if (!reVersion.exec(version || '')) {
      reject(new TypeError(`The 'from' version is invalid: ${version}`));
    }
    if (!notes) {
      reject(new TypeError('The release notes must be non-empty'));
    }

    // Create the API object
    const api = gitlab({
      url: endpoint,
      token
    });

    // Do the request
    return apiRequest(api.projects.search, 'Failed to retrieve the project list from GitLab', name, null)
      .then(projects => {
        const project = projects.filter(p => p.name === name)[0];

        if (!project) {
          throw new Error(`Failed to find the ${name} project in the GitLab projects`);
        }

        return project;
      })
      .then(project => {
        const path = `projects/${project.id}/repository/tags/v${version}/release`;
        const params = {
          // eslint-disable-next-line camelcase
          tag_name: `v${version}`, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
          description: notes
        };
        return apiRequest(api.post, `Failed to create GitLab release ${version}`, path, params);
      })
      .then(json => resolve(json));
  });
}
